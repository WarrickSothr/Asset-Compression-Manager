FROM ubuntu:20.04

LABEL maintainer="Drew Short <warrick@sothr.com>" \
    name="acm" \
    version="1.4.0" \
    description="Prepackaged ACM with defaults and tooling"

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive

ARG MOZJPEG_VERSION="3.3.1"

# Install tooling (ffmpeg, opusenc, optipng, python3)
RUN ln -fs /usr/share/zoneinfo/UCT /etc/localtime \
    && apt-get update -y \
    && apt-get install -y --fix-missing \
    curl \
    ffmpeg \
    opus-tools \
    optipng \
    python3 \
    python3-pip \
    tzdata \
    webp

WORKDIR /tmp

# Install mozjpeg
RUN curl -LS -o mozjpeg.deb "https://nexus.nulloctet.com/repository/public/mozjpeg/mozjpeg_${MOZJPEG_VERSION}_amd64.deb" \
    && dpkg -i mozjpeg.deb \
    && ln -sf /opt/mozjpeg/bin/cjpeg /bin/cjpeg \
    && rm -f mozjpeg.deb

# Cleanup image programs and cache
RUN apt-get remove -y curl \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app

# Copy application
COPY acm-config-default.json acm.py requirements.txt /app/

# Install application requirements
RUN python3 -m pip install -r requirements.txt

WORKDIR /bin

# Copy application helper script
COPY docker/acm acm

# Make script executable
RUN chmod +x acm

VOLUME ["/input", "/output"]

WORKDIR /app

COPY acm-config.json.example acm-config.json
COPY docker/entrypoint.sh .

CMD ["sh", "-c", "find /input/ -type f | /app/entrypoint.sh --stdin --remove-prefix /input/ compress -p default -d /output/"]
