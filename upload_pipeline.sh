#!/usr/bin/env bash

TEAM=docker
PIPELINE=acm

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
pushd "${DIR}"

fly -t ${TEAM} set-pipeline --pipeline ${PIPELINE} --config pipeline.yml

popd
