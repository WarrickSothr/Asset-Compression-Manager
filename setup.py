#!/usr/bin/env python

from distutils.core import setup

setup(
    name='Asset-Compression-Manager',
    version='1.4.0',
    description='Helper Utility For Managing Compressed Assets',
    author='Drew Short',
    author_email='warrick@sothr.com'
)